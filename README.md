# MCTLink #

This package extends [MATLink](http://matlink.org/) with two new commands for writing and retrieving matrices using MP format in Matlab by [Advanpix Multiprecision Computing Toolbox](http://www.advanpix.com/).

Author: Bor Plestenjak, University of Ljubljana, [bor.plestenjak@fmf.uni-lj.si](mailto:bor.plestenjak@fmf.uni-lj.si)

### Short summary ###

* MATLink is a Mathematica application for seamless two-way communication and data transfer with MATLAB.
* Advanpix Multiprecision Computing Toolbox is the MATLAB extension for computing with arbitrary precision.
* MCTLink extends MATLink with two new functions for writing and retrieving data in MP format.  

### Requirements ###

* Mathematica
* MATLAB
* MATLink must be installed in Mathematica
* MCT must be installed in MATLAB and included in the MATLAB path

### Usage ###

* Load package in Mathematica using ``<<MCTLink` `` or ``Needs["MCTLink`"]``
* MCTLink automatically loads package MATLink
* See examples in MCTLink demo.nb 

### New functions ###

| Function name | Description                    |
| ------------- | ------------------------------ |
| `MGetMP[var]`      | Imports the MATLAB variable named var of class mp in Mathematica. Works only for dense scalars, vector and matrices. If var is not of class mp, MGet is used instead.   |
| `MSetMP[var,expr]`   | Exports the value in expr and saves it in a variable named var of class mp using Precision[expr] digits in MATLAB's workspace. Works only for dense scalars, vector and matrices. If Precision[expr] is MachinePrecision, method MSet is used instead. Temporary variables named tmp_xyz_123, tmp_xyz_456, and tmp_xyz_789 are created in Matlab during transfer and deleted at the end.    |
| `MGetMPDigits[]`     | Returns the precision (number of digits) used by the MCT. The default value is 34 (quadruple precision).  |
| `MSetMPDigits[prec]`  | Sets the precision (number of digits) in MCT to prec digits. The default value is 34 (quadruple precision).   |

Other contributors: 

* Pavel Holoborodko, Advanpix LLC, http://www.advanpix.com/

FreeBSD License, see LICENSE.txt