(* :Title: MCTLink *)
(* :Context: MCTLink` *)
(* :Author: B. Plestenjak (bor.plestenjak@fmf.uni-lj.si) *)
(* :Copyright: 2017 B. Plestenjak *)
(* See the file LICENSE.txt for copying permission. *)

(* This package extends MATLink (http://matlink.org/) with two new commands for writing and retrieving matrices using MP format in Matlab by Advanpix Multiprecision Computing Toolbox (http://www.advanpix.com/). *)
(* MATLink must be installed in Mathematica and MCT must be installed in Matlab and included in the path. *)
(* Package has been tested on 64-bit Windows 10.1, using Mathematica 11.4, Matlab 2015b, MATLink 1.1, and MCT 4.3.2 Build 12144. *)

BeginPackage["MCTLink`", "MATLink`"]

MGetMP::usage = 
  "MGetMP[var] imports the MATLAB variable named \"var\" of class mp in Mathematica. Works only for dense scalars, vector and matrices. If var is not of class \"mp\", MGet is used instead.";

MSetMP::usage = 
  "MSetMP[var,expr] exports the value in expr and saves it in a variable of class mp using Precision[expr] digits named \"var\" in Matlab's workspace. Works only for dense scalars, vector and matrices. If Precision[expr] is MachinePrecision, method MSet is used instead. Temporary variables named \"tmp_xyz_123\", \"tmp_xyz_456\", and \"tmp_xyz_789\" are created in Matlab during transfer and deleted at the end.";
  
MGetMPDigits::usage = 
  "MGetMPDigits[] returns the precision (number of digits) used by the MCT. The default value is 34 (quadruple precision).";

MSetMPDigits::usage = 
  "MSetMPDigits[prec] sets the precision (number of digits) in MCT to prec digits. The default value is 34 (quadruple precision).";

Begin["`Private`"]

loadMP := MFunction["loadMP", "
  function res = loadMP(x)
  res = num2str(evalin('base', x),'%e');
  end
  ", "Overwrite" -> True]
          
digitsMP := MFunction["digitsMP", "
  function n = digitsMP(x)
  n = evalin('base',strcat('mp.Digits(',x,')'));
  end
  ", "Overwrite" -> True]
   	
isObjectMP := MFunction["isObjectMP", "
  function y = isObjectMP(x) 
  y = evalin('base',strcat('strcmpi(class(',x,'),''mp'')'));
  end
  ", "Overwrite" -> True]
   	
MGetMP[name_] := Module[{tmp},
  If[isObjectMP[name],
     tmp = SetPrecision[ImportString[loadMP[name], "table"], 
       digitsMP[name]];
     If[Max[Dimensions[tmp]] == 1, tmp[[1, 1]], 
     If[First[Dimensions[tmp]] == 1, Flatten[tmp], tmp]],
     MGet[name]
  ]]

MSetMP[name_, value_] := Module[{sM, prec},
  prec = Precision[value];
  If[prec == MachinePrecision,
    MSet[name, value],
    MEvaluate["tmp_var_123 = mp.Digits();"];
    MSet["tmp_var_456", Round[prec]];
    MEvaluate["mp.Digits(tmp_var_456);"];
    If[VectorQ[value],
      sM = ExportString[{value}, "CSV"],
      sM = ExportString[value, "CSV"]];
    MSet["tmp_var_789", sM];
    MEvaluate[name <> "=mp(tmp_var_789);"];
    MEvaluate["mp.Digits(tmp_var_123);"];
    MEvaluate["clear tmp_var_123 tmp_var_456 tmp_var_789"];
  ]]
 
MSetMPDigits[prec_Integer] := Module[{},
    MSet["tmp_var_123",prec];
    MEvaluate["mp.Digits(tmp_var_123)"];
    MEvaluate["clear tmp_var_123"];
  ];
  
MGetMPDigits[] := Module[{tmp},
    MEvaluate["tmp_var_123 = mp.Digits();"];
    tmp = MGet["tmp_var_123"];
    MEvaluate["clear tmp_var_123"];
    tmp
  ];
    
 
End[]

EndPackage[]